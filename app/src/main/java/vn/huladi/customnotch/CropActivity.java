package vn.huladi.customnotch;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CropActivity extends Activity {
    private static final int ACTION_REQUEST_GALLERY = 99;
    private static final int MAIN_ACTIVITY_REQUEST_STORAGE = RESULT_FIRST_USER;
    private ImageCropView imageCropView;
    private FrameLayout flOrigin;
    private ImageView ivCropped;
    private FrameLayout flCropped;
    private int screenWidth, screenHeight;
    private Bitmap result;
    private RecyclerView rvFrameList;
    private ImageView ivCurrentFrame;
    public View.OnClickListener onClickChoosePhoto = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");

            Intent chooser = Intent.createChooser(intent, "Choose a Picture");
            startActivityForResult(chooser, ACTION_REQUEST_GALLERY);
        }
    };
    int selectedFrame = 0;
    Bitmap currentFrame;
    public View.OnClickListener onClickSetWallpaper = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!imageCropView.isChangingScale()) {
                Bitmap temp = imageCropView.getCroppedImage();
                Log.e("cxz", "cropped  bitmap " + temp.getWidth() + " " + temp.getHeight());
                result = makeMaskImageFromBitmap(temp);
                WallpaperManager m = WallpaperManager.getInstance(CropActivity.this);
                try {
                    m.setBitmap(result);
                    Toast.makeText(CropActivity.this, "Set wallpaper done", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    Button btNext, btBack, btSetWallpaper, btChoosePhoto;
    ArrayList<String> frameList;

    //    public View.OnClickListener onClickBack = new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            btBack.setVisibility(View.GONE);
//            btSetWallpaper.setVisibility(View.GONE);
//            btNext.setVisibility(View.VISIBLE);
//            btChoosePhoto.setVisibility(View.VISIBLE);
//        }
//    };
    public View.OnClickListener onClickNextButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!imageCropView.isChangingScale()) {
                Bitmap b = imageCropView.getCroppedImage();
                Log.e("cxz", "cropped  bitmap " + b.getWidth() + " " + b.getHeight());
                bitmapConvertToFile(b);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_crop);
        imageCropView = findViewById(R.id.image);
        flOrigin = findViewById(R.id.flOrigin);
//        flCropped = findViewById(R.id.flCropped);
//        ivCropped = findViewById(R.id.ivCropped);
        rvFrameList = findViewById(R.id.rvFrameList);
        ivCurrentFrame = findViewById(R.id.ivCurrentFrame);
        btChoosePhoto = findViewById(R.id.btChoosePhoto);
        btChoosePhoto.setOnClickListener(onClickChoosePhoto);
//        btNext = findViewById(R.id.btNext);
//        btNext.setOnClickListener(onClickNextButton);
//        btBack = findViewById(R.id.btBack);
//        btBack.setOnClickListener(onClickBack);
        btSetWallpaper = findViewById(R.id.btSetWallpaper);
        btSetWallpaper.setOnClickListener(onClickSetWallpaper);
        //get screen size
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getRealSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        Bitmap imageToCrop = BitmapFactory.decodeResource(getResources(), R.mipmap.default_background);
        imageCropView.setImageBitmap(imageToCrop);

        cropFrameFromDrawable(R.drawable.frame1);
        ivCurrentFrame.setImageBitmap(currentFrame);


//        imageCropView.setImageFilePath("/storage/emulated/0/Download/DSC_0427.jpg");
//        imageCropView.setAspectRatio(9, 16);
        imageCropView.setAspectRatio(screenWidth, screenHeight);
//        makeMaskImage(ivCropped,"/storage/emulated/0/image_crop_sample/IMG_20180722094407.jpg");


        //set up list frame
        frameList = new ArrayList<>();
        for (int i = 1; i < 7; i++) {
            frameList.add("frame" + i);
        }
        FrameAdapter adapter = new FrameAdapter(this, frameList, new OnClickFrame() {
            @Override
            public void onClick(View view) {
            }

            @Override
            public void onClick(View view, int pos) {
                selectedFrame = pos;
                int id = getResources().getIdentifier(frameList.get(pos), "drawable", getPackageName());
                cropFrameFromDrawable(id);
                ivCurrentFrame.setImageBitmap(currentFrame);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvFrameList.setLayoutManager(layoutManager);
        rvFrameList.setAdapter(adapter);
    }

    void cropFrameFromDrawable(int resId) {
        //read default frame to bitmap
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inScaled = false;
        currentFrame = BitmapFactory.decodeResource(getResources(), resId, o);
        float ratio1080DivideByReal = (float) currentFrame.getWidth() / screenWidth;
        int expectHeight = (int) (screenHeight * ratio1080DivideByReal);
        if (expectHeight < currentFrame.getHeight()) {
            //you have to cut off
            currentFrame = Bitmap.createBitmap(currentFrame, 0, 0, currentFrame.getWidth(), expectHeight); //crop
            currentFrame = Bitmap.createScaledBitmap(currentFrame, screenWidth, screenHeight, false);//scale
        } else if (expectHeight > currentFrame.getHeight()) {
            //extend
            currentFrame = Bitmap.createBitmap(currentFrame, 0, 0, screenWidth, expectHeight);
            currentFrame = Bitmap.createScaledBitmap(currentFrame, screenWidth, screenHeight, false);
        }
    }

    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int columnIndex =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(columnIndex);
        }
        return result;
    }

    public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int columnIndex
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private void onScanComplete(String path) {
        Toast.makeText(CropActivity.this, "file saved " + path, Toast.LENGTH_LONG).
                show();
        flOrigin.setVisibility(View.INVISIBLE);
        flCropped.setVisibility(View.VISIBLE);

        //set for image
        Bitmap original = BitmapFactory.decodeFile(path);
        ivCropped.setImageBitmap(original);

        makeMaskImage(ivCropped, path);
    }

    public void makeMaskImage(ImageView mImageView, String path) {
        Bitmap original = BitmapFactory.decodeFile(path);
        int width = original.getWidth();
        int height = original.getHeight();
        int id = getResources().getIdentifier(frameList.get(selectedFrame), "drawable", getPackageName());
        Bitmap mask = BitmapFactory.decodeResource(getResources(), id);
        mask = Bitmap.createScaledBitmap(mask, width, height, false);
        result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, null);
        mImageView.setImageBitmap(result);
        mImageView.setScaleType(ImageView.ScaleType.CENTER);
//        mImageView.setBackgroundResource(R.drawable.frame);
    }

    Bitmap makeMaskImageFromBitmap(Bitmap original) {
        Bitmap r = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(r);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(currentFrame, 0, 0, null);
        return r;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public File bitmapConvertToFile(Bitmap bitmap) {
        FileOutputStream fileOutputStream = null;
        File bitmapFile = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory("image_crop_sample"), "");
            if (!file.exists()) {
                file.mkdir();
            }

            bitmapFile = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
            fileOutputStream = new FileOutputStream(bitmapFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            MediaScannerConnection.scanFile(this, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() {

                }

                @Override
                public void onScanCompleted(final String path, Uri uri) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CropActivity.this.onScanComplete(path);
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) {
                }
            }
        }

        return bitmapFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ACTION_REQUEST_GALLERY:
                    String filePath = "";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        filePath = getRealPathFromURI_API19(this, data.getData());
                    } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB && Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
                        filePath = getRealPathFromURI_API11to18(this, data.getData());
                    } else {
                        filePath = getRealPathFromURI_BelowAPI11(this, data.getData());
                    }

                    Uri filePathUri = Uri.parse(filePath);
                    loadAsync(filePathUri);
                    break;
            }
        }
    }

    private void loadAsync(final Uri uri) {
        CropActivity.DownloadAsync task = new CropActivity.DownloadAsync();
        task.execute(uri);

    }

    interface OnClickFrame extends View.OnClickListener {
        void onClick(View view, int pos);
    }

    class DownloadAsync extends AsyncTask<Uri, Void, Bitmap> implements DialogInterface.OnCancelListener {

        ProgressDialog mProgress;
        private Uri mUri;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgress = new ProgressDialog(CropActivity.this);
            mProgress.setIndeterminate(true);
            mProgress.setCancelable(true);
            mProgress.setMessage("Loading image...");
            mProgress.setOnCancelListener(this);
            mProgress.show();
        }

        @Override
        protected Bitmap doInBackground(Uri... params) {
            mUri = params[0];
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

            if (mProgress.getWindow() != null) {
                mProgress.dismiss();
            }

            if (mUri != null) {
                imageCropView.setImageURI(mUri);
//                Intent intent = new Intent(CropActivity.this, CropActivity.class);
//                intent.setData(mUri);
//                startActivity(intent);
            } else {
                Toast.makeText(CropActivity.this, "Failed to load image " + mUri, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            Log.i("TAG", "onProgressCancel");
            this.cancel(true);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Log.i("TAG", "onCancelled");
        }

    }
}
