package vn.huladi.customnotch;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.naver.android.helloyako.imagecrop.util.BitmapLoadUtils;

public class LoadImageAsync extends AsyncTask<Uri, Void, Bitmap> implements DialogInterface.OnCancelListener {
    ProgressDialog mProgress;
    private Uri mUri;
    Context context;

    LoadImageAsync(Context c){
        context = c;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mProgress = new ProgressDialog(context);
        mProgress.setIndeterminate(true);
        mProgress.setCancelable(true);
        mProgress.setMessage("Loading image...");
        mProgress.setOnCancelListener(this);
        mProgress.show();
    }

    @Override
    protected Bitmap doInBackground(Uri... params) {
        mUri = params[0];

        Bitmap bitmap = null;

//            while (mImageContainer.getWidth() < 1) {
//                try {
//                    Thread.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            final int w = mImageContainer.getWidth();
//            Log.d(TAG, "width: " + w);
        bitmap = BitmapLoadUtils.decode(mUri.toString(), 1000, 1000, true);
        return bitmap;
    }

//    @Override
//    protected void onPostExecute(Bitmap result) {
//        super.onPostExecute(result);

//        if (mProgress.getWindow() != null) {
//            mProgress.dismiss();
//        }
//
//        if (result != null) {
//            setImageURI(mUri, result);
//        } else {
//            Toast.makeText(MainActivity.this, "Failed to load image " + mUri, Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Log.i("TAG", "onProgressCancel");
        this.cancel(true);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.i("TAG", "onCancelled");
    }

}