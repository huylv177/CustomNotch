package vn.huladi.customnotch.model;

public class FrameItem {
    private String name;

    public FrameItem() {
    }

    public FrameItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
