package vn.huladi.customnotch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    public View.OnClickListener onClickTryAgain = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        }
    };
    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private Bitmap originBitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fullscreen);
        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);

        String originPath = "/storage/emulated/0/image_crop_sample/IMG_20180722101251.jpg";

        makeMaskImage(originPath);
    }

    public void makeMaskImage(String originPath) {
        originBitmap = BitmapFactory.decodeFile(originPath);
        iv1.setImageBitmap(originBitmap);
        Bitmap mask = BitmapFactory.decodeResource(getResources(), R.drawable.frame1);
        iv2.setImageBitmap(mask);
        Bitmap result = Bitmap.createBitmap(originBitmap.getWidth(), originBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
        mCanvas.drawBitmap(originBitmap, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        iv3.setImageBitmap(result);
    }
}
