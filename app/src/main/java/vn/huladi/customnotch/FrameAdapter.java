package vn.huladi.customnotch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

public class FrameAdapter extends RecyclerView.Adapter<FrameAdapter.FrameViewHolder> {
    private List<String> frameItemList;
    private Context context;
    CropActivity.OnClickFrame clickListener;

    FrameAdapter(Context context, List<String> frameItemList) {
        this.frameItemList = frameItemList;
        this.context = context;
    }

    FrameAdapter(Context context, List<String> frameItemList, CropActivity.OnClickFrame clickListener) {
        this.frameItemList = frameItemList;
        this.context = context;
        this.clickListener = clickListener;
    }

    public FrameAdapter(Context context, List<String> frameItemList, Uri uri) {
        this.frameItemList = frameItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public FrameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_frame, parent, false);
        return new FrameViewHolder(groceryProductView);
    }

    @Override
    public void onBindViewHolder(FrameViewHolder holder, final int position) {
        int id = context.getResources().getIdentifier(frameItemList.get(position), "drawable", context.getPackageName());
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inSampleSize = 5;
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), id, option);
//        Log.e("cxz", " width __________" + bm.getWidth() + " " + bm.getHeight());
        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), (int) (bm.getHeight() / 2.1));
        holder.item_frame_frame.setImageBitmap(bm);
        holder.item_frame_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(view, position);
            }
        });
//        holder.item_frame_back.setImageURI(uri);
    }

    @Override
    public int getItemCount() {
        return frameItemList.size();
    }

    public class FrameViewHolder extends RecyclerView.ViewHolder {
        ImageView item_frame_frame;
        ImageView item_frame_back;

        public FrameViewHolder(View view) {
            super(view);
            item_frame_frame = view.findViewById(R.id.item_frame_frame);
            item_frame_back = view.findViewById(R.id.item_frame_back);
        }
    }
}
